const express = require('express');
const app = express();
const request = require('request');
const bodyParser = require('body-parser');
const io = require('socket.io').listen(app.listen(3000));

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(express.static('./public'));
app.set('views', './view');
app.set('view engine', 'ejs');

function verifyZip(req, res, next) {
    let zip = req.body.zip || 'Brasilia';

    if(zip.length <= 2) {
        zip = 'Brasilia';
    }

    req.urlData = `http://api.openweathermap.org/data/2.5/weather?q=${zip}&appid=aa54ba6f4c7c814d6a986fdead8226cc&lang=pt`;

    next();
}

app.all('/', verifyZip, (req, res) => {
    request(req.urlData, function (error, response, body) {
        let data = JSON.parse(body);
        res.render('index', {data: data});
    });
});

console.log('server running');

io.on('connection', function(socket) {
    io.emit('chat message', 'connected');

    socket.on('chat message', function(msg){
    io.emit('chat message', msg);
    });

    socket.on('disconnect', function() {
    io.emit('chat message', 'disconnected');
    });
});