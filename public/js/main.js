(function() {
    // CHAT
    var socket = io();
    $('.chat > form').submit(function(e) {
        e.preventDefault();
        if($('#m').val().length !== 0) {
            socket.emit('chat message', $('#m').val());
            $('#m').val('');
            return false;
        }
    });
    socket.on('chat message', function(msg) {
        $('.messages').append($('<li>').text(new Date().toLocaleTimeString() + ' | ' + msg));
        var chatDiv = document.getElementsByClassName('messages')[0];
        chatDiv.scrollTop = chatDiv.scrollHeight;
    });

    $('.date').text(new Date().toLocaleDateString());
    $('.time').text(new Date().toLocaleTimeString());
    setInterval(function() {
        var textTime = new Date().toLocaleTimeString();
        var textDate = new Date().toLocaleDateString();
        $('.date').text(textDate);
        $('.time').text(textTime);
    }, 500);

    //on page load, get source JSON
    window.onload = function() {
        var getSource = new XMLHttpRequest();

        getSource.onreadystatechange = function() {
            if(getSource.readyState == XMLHttpRequest.DONE) {
                console.log(getSource.responseText);
            }
        }

        getSource.open('get', 'https://newsapi.org/v1/sources');
        getSource.send();
    }


    // populate posts XMLHttpRequest
    function populateNews(xhr) {
        var data = JSON.parse(xhr.responseText);

        for(newsArticle in data.articles) {
            data.articles[newsArticle].author = data.articles[newsArticle].author || 'Unknown';
            data.articles[newsArticle].description = data.articles[newsArticle].description || 'Access the link to read the entire article';

            var appendContent =  '<div class="newpost">';
                appendContent += '<img src="' + data.articles[newsArticle].urlToImage + '">';          
                appendContent += '<div clas="newpost-info"><h3>' + data.articles[newsArticle].title + '</h3>';
                appendContent += '<p>' + data.articles[newsArticle].description + '</p>';
                appendContent += '<span>author: ' + data.articles[newsArticle].author + '</span><br>';                
                appendContent += '<span>data: ' + new Date(data.articles[newsArticle].publishedAt).toLocaleDateString() + '</span><br>';
                appendContent += '<a href="' + data.articles[newsArticle].url + '" target="_blank">Read More</a></div>';                
                appendContent += '</div>';

            $('.news-posts').append(appendContent);
        }

    };

    // submit posts XMLHttpRequest
    $('#news-form').submit(function(e) {
        e.preventDefault();
        var source = $('#source').val();
        var sort = $('#sort').val();


        var newsRequest = new XMLHttpRequest();

        newsRequest.onreadystatechange = function() {
            if (newsRequest.readyState == XMLHttpRequest.DONE) {
                // remove all existing content 
                $('.news-posts').empty();
                populateNews(newsRequest);
            }
        };

        newsRequest.open('get', 'https://newsapi.org/v1/articles?source='+ source +'&sortBy=' + sort + '&apiKey=b9b9f321fa38480b923f36a8b555f305');
        newsRequest.send();

    });
})($);